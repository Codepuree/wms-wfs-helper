""" This is a network test. """
from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QApplication
from PyQt5.QtNetwork import QNetworkAccessManager, QNetworkRequest
import sys

def printContent():
    answerAsText = bytes(replyObject.readAll()).decode("utf-8")
    print('Answer as Text:\n', answerAsText)

if __name__ == '__main__':
    app = QApplication(sys.argv)

    # sUrl = "http://www.example.com"
    sUrl = 'https://geodienste.hamburg.de/HH_WMS_Gruenflaechen?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities'
    url = QUrl(sUrl)

    request = QNetworkRequest()
    request.setUrl(url)
    manager = QNetworkAccessManager()

    replyObject = manager.get(request)
    replyObject.finished.connect(printContent)

    sys.exit(app.exec_())
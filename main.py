""" This is a helper to learn about WMS, WFS services. """

import sys
from mainWindow import Ui_MainWindow
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit
from PyQt5.QtGui import QPixmap
from urllib.request import urlopen
from urllib.parse import urlsplit, urlparse, parse_qsl, urlencode
import xml.dom.minidom
from time import sleep


def main(argv):
    app = QApplication(argv)
    mainwin = QMainWindow()

    mw = Ui_MainWindow()
    mw.setupUi(mainwin)
    mainwin.show()

    def getParameter():
        base = mw.formLayout.itemAt(0, 1).widget().text()
        parameters = []
        for i in range(1, mw.formLayout.rowCount()):
            parameters.append((
                mw.formLayout.itemAt(i, 0).widget().text(),
                mw.formLayout.itemAt(i, 1).widget().text()
            ))

        out = '{}?{}'.format(base, urlencode(parameters))

        if 'STYLES' not in out:
            out += '&STYLES='
        return out

    def urlToParameters(url):
        spUrl = urlsplit(url)

        base = '{}://{}{}'.format(spUrl.scheme, spUrl.netloc, spUrl.path)
        mw.formLayout.addRow('Basis URL:', QLineEdit(base))

        query = parse_qsl(spUrl.query)
        for vp in query:
            mw.formLayout.addRow(QLabel(vp[0]), QLineEdit(vp[1]))
    
    def deleteAll():
        if mw.formLayout.rowCount > 0:
            for i in reversed(range(mw.formLayout.rowCount)):
                mw.formLayout.removeRow(i)

    def onBtnSearchClick():
        pixmap = QPixmap()
        url = mw.leURL.text()
        response = urlopen(url)
        resInfo = response.info()
        print('Res Info:', resInfo.get_content_type(), resInfo.get_content_maintype(), resInfo.get_content_subtype())
        data = response.read()

        if resInfo.get_content_maintype() == 'image':
            pixmap.loadFromData(data)
            mw.lblImg.setPixmap(pixmap)
            mw.teData.clear()
        else:
            data = data.decode("utf-8").replace('\r', '\n').replace('\n\n', '\n')
            mw.lblImg.clear()
            xml_data = xml.dom.minidom.parseString(data)
            pretty_xml_as_string = xml_data.toprettyxml()
            mw.teData.setText(pretty_xml_as_string)
        # urlToParameters(url)
        print('Query: ', getParameter())

    mw.btnSearch.clicked.connect(onBtnSearchClick)

    def onBtnAddClick():
        parameter = mw.leParameter.text().strip().upper()
        value = mw.leValue.text().strip()

        if len(parameter) > 1 and len(value) > 1:
            mw.formLayout.addRow(parameter, QLineEdit(value))
    
    mw.btnAdd.clicked.connect(onBtnAddClick)

    def onBtnUpdateClick():
        mw.leURL.setText(getParameter())
    
    mw.btnUpdate.clicked.connect(onBtnUpdateClick)

    def onUrlChanged():
        # deleteAll()
        for i in reversed(range(mw.formLayout.rowCount())):
            mw.formLayout.removeRow(i)
        sleep(2)
        urlToParameters(mw.leURL.text())

    mw.leURL.textChanged.connect(onUrlChanged)

    mw.leURL.setText('https://geodienste.hamburg.de/HH_WMS_Gruenflaechen?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=false&t=721&zufall=0.26910934951484355&LAYERS=spielplaetze%2Cplaetze%2Cparkanlagen&WIDTH=128&HEIGHT=128&CRS=EPSG%3A25832&BBOX=564667.0175251439%2C5928157.68565485%2C567482.1826716214%2C5930972.850801328&STYLES=')

    sys.exit(app.exec_())


if __name__ == '__main__':
    main(sys.argv)

# WMS WFS - helper

![](./WMS_WFS-helper.jpg)

Der "_WMS WFS - helper_" ist in Python mit PyQt5 geschrieben und hilft beim Verstehen von "_Web Map Service_" und "_Web Feature Service_".

## Dependencies

- PyQt5

## How to start

```bash
> python main.py
```

## Future

- Erstellen eines Executables (.exe)
- Beschleunigung
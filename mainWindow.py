# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\GUI\mainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(886, 719)
        icon = QtGui.QIcon.fromTheme("light")
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.leURL = QtWidgets.QLineEdit(self.centralwidget)
        self.leURL.setObjectName("leURL")
        self.horizontalLayout.addWidget(self.leURL)
        self.btnUpdate = QtWidgets.QPushButton(self.centralwidget)
        self.btnUpdate.setObjectName("btnUpdate")
        self.horizontalLayout.addWidget(self.btnUpdate)
        self.btnSearch = QtWidgets.QPushButton(self.centralwidget)
        self.btnSearch.setObjectName("btnSearch")
        self.horizontalLayout.addWidget(self.btnSearch)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 866, 294))
        self.scrollAreaWidgetContents.setMaximumSize(QtCore.QSize(866, 310))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.gridLayout = QtWidgets.QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.gridLayout.addLayout(self.formLayout, 0, 0, 1, 1)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout.addWidget(self.scrollArea)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout_5.addWidget(self.label)
        self.leParameter = QtWidgets.QLineEdit(self.centralwidget)
        self.leParameter.setObjectName("leParameter")
        self.horizontalLayout_5.addWidget(self.leParameter)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_5.addWidget(self.label_2)
        self.leValue = QtWidgets.QLineEdit(self.centralwidget)
        self.leValue.setObjectName("leValue")
        self.horizontalLayout_5.addWidget(self.leValue)
        self.btnAdd = QtWidgets.QPushButton(self.centralwidget)
        self.btnAdd.setObjectName("btnAdd")
        self.horizontalLayout_5.addWidget(self.btnAdd)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.lblImg = QtWidgets.QLabel(self.centralwidget)
        self.lblImg.setMinimumSize(QtCore.QSize(256, 256))
        self.lblImg.setAlignment(QtCore.Qt.AlignCenter)
        self.lblImg.setObjectName("lblImg")
        self.horizontalLayout_2.addWidget(self.lblImg)
        self.teData = QtWidgets.QTextEdit(self.centralwidget)
        self.teData.setObjectName("teData")
        self.horizontalLayout_2.addWidget(self.teData)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 886, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "WMS WFS helper"))
        self.btnUpdate.setText(_translate("MainWindow", "Update"))
        self.btnSearch.setText(_translate("MainWindow", "Ausführen"))
        self.label.setText(_translate("MainWindow", "Parameter:"))
        self.label_2.setText(_translate("MainWindow", "Wert:"))
        self.btnAdd.setText(_translate("MainWindow", "Add"))
        self.lblImg.setText(_translate("MainWindow", "TextLabel"))

